const asyncErrorHandle = (callback) => {
  return (res, req, next) => {
    return callback(res, req, next)
        .catch(next);
  };
};

const getArrayValueNextToCurrentOne = (currentValue, array = []) => {
  if (!array.length) throw new Error('Array shouldn`t by empty');
  if (!currentValue) return array[0];

  const indexOfCurrentValue = array.findIndex((el) => el === currentValue);
  if (indexOfCurrentValue === array.length - 1) return currentValue;

  return array[indexOfCurrentValue + 1];
};

module.exports = {
  asyncErrorHandle,
  getArrayValueNextToCurrentOne,
};
