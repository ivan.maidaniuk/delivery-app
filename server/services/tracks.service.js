const Truck = require('../models/truck.model');
const NotFoundError = require('../utils/errors/NotFoundError');
const mongoose = require('mongoose');
const {Trucks, TruckStatuses} = require('../../shared/Trucks');

/**
 * Create new truck
 * @param {object} obj
 * @param {string} obj.userId
 * @param {string} obj.truckType
 * */
async function createTrack({userId, truckType}) {
  const truck = new Truck({
    type: truckType,
    created_by: userId,
  });
  await truck.save();
}

/**
 * Get truck by userId
 * @param {object} userId
 * @param {object} options
 * @param {number} options.offset
 * @param {number} options.limit
 * */
async function getTrucksByUserId(userId, {offset = 0, limit = 0}) {
  const trucks = await Truck.find({created_by: userId})
      .skip(offset)
      .limit(limit);
  return trucks;
}

/**
 * Get user assigned truck by userId
 * @param {string} userId
 * */
async function getUserAssignedTruck(userId) {
  const truck = Truck.find({assigned_to: userId});
  if (!truck) {
    throw new NotFoundError(`Truck assigned to user with ${userId} id wasn't found`);
  }
  return truck;
}


/**
 * Get user's truck by id
 * @param {object} obj
 * @param {string} obj.userId
 * @param {string} obj.truckId
 * */
async function getUserTruckByTruckId({userId, truckId}) {
  const truck = await Truck.findOne({created_by: userId, _id: truckId});
  if (!truck) {
    throw new NotFoundError(`Truck with ${truckId} id wasn't found`);
  }
  return truck;
}


/**
 * Update truck type by truck id
 * @param {object} obj
 * @param {string} obj.userId
 * @param {string} obj.truckId
 * @param {string} obj.newTruckType
 * */
async function updateTruckTypeById({userId, truckId, newTruckType}) {
  const truck = await Truck.findOne({created_by: userId, _id: truckId});
  if (!truck) {
    throw new NotFoundError(`Truck with ${truckId} id wasn't found`);
  }
  truck.type = newTruckType;
  await truck.save();
}

/**
 * Delete truck by id
 * @param {object} obj
 * @param {string} obj.userId
 * @param {string} obj.truckId
 * */
async function deleteTruckById({userId, truckId}) {
  const truck = await Truck.findOne({created_by: userId, _id: truckId});
  if (!truck) {
    throw new NotFoundError(`Truck with ${truckId} id wasn't found`);
  }
  await truck.remove();
}

/**
 * Delete truck by id
 * @param {object} obj
 * @param {string} obj.userId
 * @param {string} obj.truckId
 * */
async function assignTruckToUser({userId, truckId}) {
  const truck = await Truck.findOne({created_by: userId, _id: truckId});
  if (!truck) {
    throw new NotFoundError(`Truck with ${truckId} id wasn't found`);
  }
  if (truck.assigned_to && truck.assigned_to.toString() === userId) {
    return;
  }
  await Truck.updateMany({created_by: userId}, {$set: {assigned_to: null}});
  truck.assigned_to = new mongoose.Types.ObjectId(userId);
  await truck.save();
}

/**
 * Change truck status
 * @param {object} obj
 * @param {string} obj.userId
 * @param {string} obj.truckStatus
 * */
async function changeTruckStatus({userId, truckStatus}) {
  Truck.findOneAndUpdate({assigned_to: userId}, {status: truckStatus});
}


/**
 * Find appropriate truck according to requirements
 * @param {object} requirementsObj
 * @param {object} requirementsObj.dimensions
 * @param {string} requirementsObj.payload
 * */
async function findTruckForLoad({dimensions, payload}) {
  const appropriateTrucks = getAppropriateTrucksNames({dimensions, payload});
  console.log(appropriateTrucks);
  const truck = await Truck.findOne({
    status: TruckStatuses.IS,
    type: {$in: appropriateTrucks},
  });
  console.log({truck});
  if (!truck) {
    throw new NotFoundError(`There is no acceptable truck`);
  }
  return truck;
}

/**
 * Get appropriate trucks
 * @param {object} requirementsObj
 * @param {object} requirementsObj.dimensions
 * @param {string} requirementsObj.payload
 * @return {array}
 * */
function getAppropriateTrucksNames({dimensions, payload}) {
  return Object.entries(Trucks).map(([truckName, truckParams]) => {
    if (truckParams.width > dimensions.width &&
        truckParams.length > dimensions.length &&
        truckParams.height > dimensions.height &&
        truckParams.payload > payload) {
      return truckName;
    }
  });
}


module.exports = {
  createTrack,
  getTrucksByUserId,
  getUserTruckByTruckId,
  updateTruckTypeById,
  deleteTruckById,
  assignTruckToUser,
  changeTruckStatus,
  getUserAssignedTruck,
  findTruckForLoad,
};
