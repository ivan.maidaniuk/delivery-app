const Load = require('../models/load.model');
const mongoose = require('mongoose');
const {
  changeTruckStatus,
  findTruckForLoad,
} = require('./tracks.service');
const {TruckStatuses} = require('../../shared/Trucks');
const {getArrayValueNextToCurrentOne} = require('../utils/app.util');
const {LoadStatuses, LoadStates} = require('../../shared/Loads');
const {
  NotFoundError,
  InternalServerError,
} = require('../utils/errors');
const {Roles} = require('../../shared/Roles');

/**
 * @typedef load
 * @property name
 * @property payload
 * @property pickup_address
 * @property delivery_address
 * @property dimensions
 * */

/**
 * Create new load
 * @param {object} obj
 * @param {string} obj.userId
 * @param {load} obj.load
 * */
async function createNewLoad({userId, load}) {
  const truck = new Load({
    created_by: userId,
    name: load.name,
    payload: load.payload,
    pickup_address: load.pickup_address,
    delivery_address: load.delivery_address,
    dimensions: {
      width: load.dimensions.width,
      length: load.dimensions.length,
      height: load.dimensions.height,
    },
  });
  console.log(truck);
  await truck.save();
}

/**
 * Get loads according to user role
 * @param {object} obj
 * @param {string} obj.userId
 * @param {string} obj.role
 * @param {object} obj.options
 * @param {number} obj.options.offset
 * @param {number} obj.options.limit
 * @param {string} obj.options.status
 * */
async function getLoads({userId, role, options: {offset = 0, limit = 0, status = 'ANY'}}) {
  let query = {
    status: status.match(/ANY/i) ? {$in: [...Object.keys(LoadStatuses)]} : status.toUpperCase(),
  };

  if (role === Roles.DRIVER) {
    query = {...query, assigned_to: userId};
  } else if (role === Roles.SHIPPER) {
    query = {...query, created_by: userId};
  } else {
    throw new InternalServerError(`User role ${role} is not handled`);
  }

  return Load.find(query)
      .skip(offset)
      .limit(limit);
}

/**
 * Get loads count according to user role
 * @param {object} obj
 * @param {string} obj.userId
 * @param {string} obj.role
 * */
async function getLoadsCount({userId, role}) {
  if (role === Roles.DRIVER) {
    return Load.countDocuments({assigned_to: userId});
  } else if (role === Roles.SHIPPER) {
    return Load.countDocuments({created_by: userId});
  }
  throw new InternalServerError(`User role ${role} is not handled`);
}

/**
 * Get loads count according to user role
 * @param {object} obj
 * @param {string} obj.userId
 * @param {string} obj.role
 * @param {string} obj.loadId
 * */
async function getLoadById({userId, role, loadId}) {
  let query = {};
  if (role === Roles.DRIVER) {
    query = {...query, assigned_to: userId};
  } else if (role === Roles.SHIPPER) {
    query = {...query, created_by: userId};
  } else {
    throw new InternalServerError(`User role ${role} is not handled`);
  }
  const load = await Load.findOne({...query, _id: loadId});
  if (!load) {
    throw new NotFoundError(`There is no load with id ${loadId}`);
  }
  return load;
}

/**
 * Get loads count according to user role
 * @param {object} obj
 * @param {string} obj.userId
 * */
async function getActiveLoad({userId}) {
  const load = await Load.findOne({assigned_to: userId});
  if (!load) {
    throw new NotFoundError(`There is no active load assigned to user with ${userId} id`);
  }
  return load;
}

/**
 * Get loads count according to user role
 * @param {object} obj
 * @param {string} obj.userId
 * */
async function iterateToNextLoadState({userId}) {
  const load = await Load.findOne({assigned_to: userId});
  if (!load) {
    throw new NotFoundError(`There wasn't found any load assigned to user with ${userId} id`);
  }

  const loadStates = Object.values(LoadStates);
  const currentState = load.state;
  const nextLoadState = getArrayValueNextToCurrentOne(currentState, loadStates);
  load.state = nextLoadState;
  if (currentState === nextLoadState) {
    load.status = LoadStatuses.SHIPPED;
    await changeTruckStatus({userId, truckStatus: TruckStatuses.IS});
  }
  load.save();
  return nextLoadState;
}

/**
 * Post load
 * @param {object} obj
 * @param {string} obj.loadId
 * @param {string} obj.loadStatus
 * */
async function changeLoadStatus({loadId, loadStatus}) {
  const load = await Load.findOne({_id: loadId});
  if (!load) {
    throw new NotFoundError(`Load with ${loadId} id wasn't found`);
  }
  load.status = loadStatus;
  load.save();
}


/**
 * Post load and search for driver
 * @param {object} obj
 * @param {string} obj.loadId
 * @param {string} obj.loadId
 * */
async function searchDriverForLoad({loadId}) {
  const load = await Load.findOne({_id: loadId});
  if (!load) {
    throw new NotFoundError(`Load with ${loadId} id wasn't found`);
  }

  const acceptableTruck = await findTruckForLoad({
    dimensions: {...load.dimensions},
    payload: load.payload,
  });
  acceptableTruck.status = TruckStatuses.OL;
  load.assigned_to = new mongoose.Types.ObjectId(acceptableTruck.assigned_to);
  load.state = Object.values(LoadStates)[0];
  load.status = LoadStatuses.ASSIGNED;

  acceptableTruck.save();
  load.save();
}


module.exports = {
  createNewLoad,
  getLoadsCount,
  getLoads,
  getActiveLoad,
  iterateToNextLoadState,
  getLoadById,
  searchDriverForLoad,
  changeLoadStatus,
};
