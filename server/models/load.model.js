const mongoose = require('mongoose');
const Joi = require('joi');
const {LoadStatuses} = require('../../shared/Loads');
const {applyJoiValidationAfterCommonOne} = require('./model.validation');

const {JoiLoadSchemas} = require('../../shared/validations.schema');


const LoadSchema = new mongoose.Schema({
  name: String,
  payload: Number,
  pickup_address: String,
  delivery_address: String,
  dimensions: {
    type: Object,
    width: Number,
    length: Number,
    height: Number,
  },
  created_by: {
    required: true,
    type: mongoose.Types.ObjectId,
  },
  assigned_to: {
    type: mongoose.Types.ObjectId,
    default: null,
  },
  status: {
    type: String,
    default: Object.values(LoadStatuses)[0],
  },
  state: {
    type: String,
    default: null,
  },
  logs: {
    type: Array,
    default: [],
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

const JoiLoadSchema = Joi.object({
  status: JoiLoadSchemas.statuses,
  state: JoiLoadSchemas.states,
  name: JoiLoadSchemas.name.required(),
  payload: JoiLoadSchemas.payload.required(),
  pickup_address: JoiLoadSchemas.pickup_address.required(),
  delivery_address: JoiLoadSchemas.delivery_address.required(),
  dimensions: JoiLoadSchemas.dimensions.required(),
  logs: JoiLoadSchemas.logs,
});

applyJoiValidationAfterCommonOne({
  mongooseSchema: LoadSchema,
  joiSchema: JoiLoadSchema,
  validationOptions: {
    allowUnknown: true,
  },
});

const Load = mongoose.model('Load', LoadSchema);

module.exports = Load;
