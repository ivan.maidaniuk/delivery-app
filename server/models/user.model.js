const mongoose = require('mongoose');
const Joi = require('joi');
const {applyJoiValidationAfterCommonOne} = require('./model.validation');

const {JoiSchemas} = require('../../shared/validations.schema');
const {
  BadRequestError,
} = require('../utils/errors');


const UserSchema = new mongoose.Schema({
  role: String,
  email: {
    type: String,
    unique: true,
  },
  password: String,
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

const JoiUserSchema = Joi.object({
  role: JoiSchemas.role.required(),
  email: JoiSchemas.email.required(),
  password: JoiSchemas.encryptedPassword.required(),
});

applyJoiValidationAfterCommonOne({
  mongooseSchema: UserSchema,
  joiSchema: JoiUserSchema,
  validationOptions: {
    allowUnknown: true,
  },
});

UserSchema.post('save', function(error, doc, next) {
  if (error.name === 'MongoError' && error.code === 11000) {
    next(new BadRequestError('Email must be unique'));
  } else {
    next(error);
  }
});

const User = mongoose.model('User', UserSchema);

module.exports = User;
