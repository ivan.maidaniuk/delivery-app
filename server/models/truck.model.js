const mongoose = require('mongoose');
const Joi = require('joi');
const {TruckStatuses} = require('../../shared/Trucks');
const {applyJoiValidationAfterCommonOne} = require('./model.validation');

const {JoiTruckSchemas} = require('../../shared/validations.schema');


const TruckSchema = new mongoose.Schema({
  created_by: {
    required: true,
    type: mongoose.Types.ObjectId,
  },
  assigned_to: {
    type: mongoose.Types.ObjectId,
    default: null,
  },
  type: String,
  status: {
    type: String,
    default: Object.keys(TruckStatuses)[0],
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

const JoiTruckSchema = Joi.object({
  type: JoiTruckSchemas.type.required(),
  status: JoiTruckSchemas.status.required(),
});

applyJoiValidationAfterCommonOne({
  mongooseSchema: TruckSchema,
  joiSchema: JoiTruckSchema,
  validationOptions: {
    allowUnknown: true,
  },
});

const Truck = mongoose.model('Truck', TruckSchema);

module.exports = Truck;
