const {InternalServerError} = require('../utils/errors');

/**
 * Validate data with Joi after native mongoose input-validations
 * @param {object} obj
 * @param {object} obj.mongooseSchema
 * @param {object} obj.joiSchema
 * @param {object} obj.validationOptions
 * */
function applyJoiValidationAfterCommonOne({mongooseSchema, joiSchema, validationOptions = {}}) {
  mongooseSchema.post('validate', function(doc) {
    const {error} = joiSchema.validate(doc, {...validationOptions});
    if (error) {
      throw new InternalServerError(error.message);
    }
  });
}

module.exports = {
  applyJoiValidationAfterCommonOne,
};
