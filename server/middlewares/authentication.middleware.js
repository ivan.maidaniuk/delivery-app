const {AccessDeniedError} = require('../utils/errors');

/**
 * Authentication middleware by user role
 * @param {...*} roles
 * @return {Function}
 */
function authenticationMiddleware(...roles) {
  return function(req, res, next) {
    if (!roles.includes(req.user.role)) {
      next(new AccessDeniedError());
    }
    next();
  };
}

module.exports = {
  authenticationMiddleware,
};
