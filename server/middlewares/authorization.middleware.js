const expressJwt = require('express-jwt');
const config = require('config');
const {AccessDeniedError} = require('../utils/errors');
const {asyncErrorHandle} = require('../utils/app.util');

const jwtSecret = config.get('jwtSecret');

/**
 * Authorization middleware
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 */
async function authorizationMiddleware(req, res, next) {
  await expressJwt({
    secret: jwtSecret,
    algorithms: ['HS256'],
    getToken: (req) => getJwtTokenFromAuthHeaderOrFromCookie(req),
  })(req, res, next);
}

/**
 * Get Jwt token from Authentication header or from cookies
 * @param {object} req
 * @return {string | AccessDeniedError}
 * */
function getJwtTokenFromAuthHeaderOrFromCookie(req) {
  const tokenFormAuthHeader = getJwtTokenFromAuthHeader(req);
  if (tokenFormAuthHeader) {
    return tokenFormAuthHeader;
  }

  const tokenFromCookie = req.cookies?.token;
  if (tokenFromCookie) {
    return tokenFromCookie;
  }

  throw new AccessDeniedError(
      'There is no Jwt token applied. ' +
      'Attach Jwt token to \'Authentication\' header of request.',
  );
}

/**
 * Get Jwt token from Authentication header
 * @param {object} req
 * @return {string | undefined}
 * */
function getJwtTokenFromAuthHeader(req) {
  const authHeader = req.headers.authorization;
  if (!authHeader) {
    return;
  }
  const [type, token] = authHeader.split(' ');
  if (authHeader && type === 'Bearer' || type === 'JWT') {
    return token;
  }
}


module.exports = {
  authorizationMiddleware: asyncErrorHandle(authorizationMiddleware),
};
