const Joi = require('joi');
const {JoiSchemas, JoiLoadSchemas} = require('../../../shared/validations.schema');

const {asyncErrorHandle} = require('../../utils/app.util');


/**
 * Validate data for load creation
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 * */
async function validateLoadCreateInput(req, res, next) {
  const schema = Joi.object({
    name: JoiLoadSchemas.name.required(),
    payload: JoiLoadSchemas.payload.required(),
    pickup_address: JoiLoadSchemas.pickup_address.required(),
    delivery_address: JoiLoadSchemas.delivery_address.required(),
    dimensions: JoiLoadSchemas.dimensions.required(),
  });
  await schema.validateAsync(req.body);
  next();
}

/**
 * Validate query for load getting/search
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 * */
async function validateGetLoadsQuery(req, res, next) {
  const schema = Joi.object({
    limit: JoiSchemas.limit,
    offset: JoiSchemas.offset,
    status: JoiLoadSchemas.statuses,
  });
  await schema.validateAsync(req.query);
  next();
}


module.exports = {
  validateLoadCreateInput: asyncErrorHandle(validateLoadCreateInput),
  validateGetLoadsQuery: asyncErrorHandle(validateGetLoadsQuery),
};
