const Joi = require('joi');
const {JoiSchemas} = require('../../../shared/validations.schema');
const {JoiTruckSchemas} = require('../../../shared/validations.schema');

const {asyncErrorHandle} = require('../../utils/app.util');


/**
 * Validate type of truck
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 * */
async function validateTruckTypeInput(req, res, next) {
  const schema = Joi.object({
    type: JoiTruckSchemas.type.required(),
  });
  await schema.validateAsync(req.body);
  next();
}

/**
 * Validate query for trucks getting/search
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 * */
async function validateGetTrucksQuery(req, res, next) {
  const schema = Joi.object({
    limit: JoiSchemas.limit,
    offset: JoiSchemas.offset,
  });
  await schema.validateAsync(req.query);
  next();
}


module.exports = {
  validateTruckTypeInput: asyncErrorHandle(validateTruckTypeInput),
  validateGetTrucksQuery: asyncErrorHandle(validateGetTrucksQuery),
};
