const Joi = require('joi');

const {asyncErrorHandle} = require('../../utils/app.util');
const {JoiSchemas} = require('../../../shared/validations.schema');

/**
 * Validate registration data
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 * */
async function validateRegistrationInput(req, res, next) {
  const schema = Joi.object({
    email: JoiSchemas.email.required(),
    password: JoiSchemas.password.required(),
    role: JoiSchemas.role.required(),
  });
  await schema.validateAsync(req.body);
  next();
}

/**
 * Validate login data
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 * */
async function validateLoginInput(req, res, next) {
  const schema = Joi.object({
    email: JoiSchemas.email.required(),
    password: JoiSchemas.password.required(),
  });
  await schema.validateAsync(req.body);
  next();
}

module.exports = {
  validateRegistrationInput: asyncErrorHandle(validateRegistrationInput),
  validateLoginInput: asyncErrorHandle(validateLoginInput),
};
