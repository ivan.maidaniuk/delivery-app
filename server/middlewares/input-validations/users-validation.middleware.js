const Joi = require('joi');
const {JoiSchemas} = require('../../../shared/validations.schema');

const {asyncErrorHandle} = require('../../utils/app.util');


/**
 * Validate data for changing input
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 * */
async function validateChangePasswordInput(req, res, next) {
  const schema = Joi.object({
    oldPassword: JoiSchemas.password.required(),
    newPassword: JoiSchemas.password.required(),
  });
  await schema.validateAsync(req.body);
  next();
}


module.exports = {
  validateChangePasswordInput: asyncErrorHandle(validateChangePasswordInput),
};
