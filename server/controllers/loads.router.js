const express = require('express');
const loadsRouter = new express.Router();

const {
  getLoads,
  getLoadsCount,
  createNewLoad,
  getActiveLoad,
  iterateToNextLoadState,
  getLoadById,
  searchDriverForLoad,
  changeLoadStatus,
} = require('../services/loads.service');
const {getUserAssignedTruck} = require('../services/tracks.service');
const {Roles} = require('../../shared/Roles');
const {LoadStatuses} = require('../../shared/Loads');
const {asyncErrorHandle} = require('../utils/app.util');
const {authenticationMiddleware} = require('../middlewares/authentication.middleware');
const {
  validateGetLoadsQuery,
  validateLoadCreateInput,
} = require('../middlewares/input-validations/loads-validation.middleware');

loadsRouter.get('/',
    [authenticationMiddleware(Roles.DRIVER, Roles.SHIPPER), validateGetLoadsQuery],
    asyncErrorHandle(async (req, res) => {
      const userId = req.user._id;
      const userRole = req.user.role;
      const {limit = 0, offset = 0, status = 'ANY'} = req.query;
      const loads = await getLoads({
        userId, role: userRole, options: {
          limit: +limit,
          offset: +offset,
          status,
        },
      });
      res.send({
        loads,
        limit,
        offset,
        status,
        count: await getLoadsCount({userId, role: userRole}),
      });
    }),
);

loadsRouter.post('/',
    [authenticationMiddleware(Roles.SHIPPER), validateLoadCreateInput],
    asyncErrorHandle(async (req, res) => {
      await createNewLoad({userId: req.user._id, load: req.body});
      res.send({message: 'Load created successfully'});
    }),
);

loadsRouter.get('/active',
    authenticationMiddleware(Roles.DRIVER),
    asyncErrorHandle(async (req, res) => {
      res.send({load: await getActiveLoad({userId: req.user._id})});
    }),
);

loadsRouter.patch('/active/state',
    authenticationMiddleware(Roles.DRIVER),
    asyncErrorHandle(async (req, res) => {
      const currentState = await iterateToNextLoadState({userId: req.user._id});
      res.send({message: `Load state changed to ${currentState}`});
    }),
);

loadsRouter.get('/:id',
    asyncErrorHandle(async (req, res) => {
      res.send({
        load: await getLoadById({
          userId: req.user._id,
          role: req.user.role,
          loadId: req.params.id,
        }),
      });
    }),
);

loadsRouter.put('/:id',
    authenticationMiddleware(Roles.SHIPPER),
    asyncErrorHandle(async (req, res) => {
      // Update user's load by id (available only for shipper role)
    }),
);

loadsRouter.delete('/:id',
    authenticationMiddleware(Roles.SHIPPER),
    asyncErrorHandle(async (req, res) => {
      // Delete user's load by id (available only for shipper role)
    }),
);

loadsRouter.post('/:id/post',
    authenticationMiddleware(Roles.SHIPPER),
    asyncErrorHandle(async (req, res) => {
      const loadId = req.params.id;
      await changeLoadStatus({loadId, loadStatus: LoadStatuses.POSTED});
      await searchDriverForLoad({loadId});
      res.send({
        message: 'Load posted successfully',
        driver_found: true,
      });
    }),
);

loadsRouter.get('/:id/shipping_info',
    authenticationMiddleware(Roles.SHIPPER),
    asyncErrorHandle(async (req, res) => {
      const load = await getLoadById({
        userId: req.user._id,
        role: req.user.role,
        loadId: req.params.id,
      });
      const truck = await getUserAssignedTruck(load.assigned_to);
      res.send({
        load,
        truck,
      });
    }),
);

module.exports = {
  loadsRouter,
};
