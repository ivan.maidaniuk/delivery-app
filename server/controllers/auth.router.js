const express = require('express');
const User = require('../models/user.model');
const authRouter = new express.Router();

const {asyncErrorHandle} = require('../utils/app.util');
const {
  validateRegistrationInput,
  validateLoginInput,
} = require('../middlewares/input-validations/auth-validation.middleware');
const {
  registerUser,
  createAndGetJwtToken,
  validateUserCredentials,
} = require('../services/auth.service');


authRouter.post('/register', validateRegistrationInput, asyncErrorHandle(async (req, res) => {
  await registerUser(req.body);
  res.send({message: 'Profile created successfully'});
}));

authRouter.post('/login', validateLoginInput, asyncErrorHandle(async (req, res) => {
  const {email, password} = req.body;
  const userDoc = await User.findOne({email});
  await validateUserCredentials({userDoc, password});
  const jwtToken = await createAndGetJwtToken(userDoc);
  res.cookie('token', jwtToken, {httpOnly: true});
  res.send({jwt_token: jwtToken});
}));

module.exports = {
  authRouter,
};
