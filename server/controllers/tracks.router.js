const express = require('express');
const Truck = require('../models/truck.model');
const tracksRouter = new express.Router();

const {
  createTrack,
  getTrucksByUserId,
  getUserTruckByTruckId,
  deleteTruckById,
  updateTruckTypeById,
  assignTruckToUser,
} = require('../services/tracks.service');
const {asyncErrorHandle} = require('../utils/app.util');
const {
  validateTruckTypeInput,
  validateGetTrucksQuery,
} = require('../middlewares/input-validations/trucks-validation.middleware');

tracksRouter.get('/', validateGetTrucksQuery, asyncErrorHandle(async (req, res) => {
  const userId = req.user._id;
  const {offset = 0, limit = 0} = req.query;
  const trucks = await getTrucksByUserId(userId, {
    offset: +offset,
    limit: +limit,
  });
  res.send({
    trucks,
    offset,
    limit,
    count: await Truck.countDocuments({created_by: userId}),
  });
}));

tracksRouter.post('/', validateTruckTypeInput, asyncErrorHandle(async (req, res) => {
  const {type: truckType} = req.body;
  await createTrack({userId: req.user._id, truckType});
  res.send({message: 'Truck created successfully'});
}));

tracksRouter.get('/:id', asyncErrorHandle(async (req, res) => {
  const userId = req.user._id;
  const truckId = req.params.id;
  const truck = await getUserTruckByTruckId({userId, truckId});
  res.send({truck});
}));

tracksRouter.put('/:id', validateTruckTypeInput, asyncErrorHandle(async (req, res) => {
  const userId = req.user._id;
  const truckId = req.params.id;
  const {type: newTruckType} = req.body;
  await updateTruckTypeById({userId, truckId, newTruckType});
  res.send({message: 'Truck details changed successfully'});
}));

tracksRouter.delete('/:id', asyncErrorHandle(async (req, res) => {
  const userId = req.user._id;
  const truckId = req.params.id;
  await deleteTruckById({userId, truckId});
  res.send({message: 'Truck deleted successfully'});
}));

tracksRouter.post('/:id/assign', asyncErrorHandle(async (req, res) => {
  const userId = req.user._id;
  const truckId = req.params.id;
  await assignTruckToUser({userId, truckId});
  res.send({message: 'Truck assigned successfully'});
}));

module.exports = {
  tracksRouter,
};
