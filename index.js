const express = require('express');
const app = express();

const config = require('config');
const morgan = require('morgan');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');

const dbConnectionUrl = config.get('dbConnectionUrl');
const port = config.get('devServer.port') || 8080;

const {Roles} = require('./shared/Roles');
const {NotFoundError} = require('./server/utils/errors');
const {asyncErrorHandle} = require('./server/utils/app.util');
const {authenticationMiddleware} = require('./server/middlewares/authentication.middleware');
const {authorizationMiddleware} = require('./server/middlewares/authorization.middleware');
const {authRouter} = require('./server/controllers/auth.router');
const {userRouter} = require('./server/controllers/users.router');
const {tracksRouter} = require('./server/controllers/tracks.router');
const {loadsRouter} = require('./server/controllers/loads.router');


app.use(morgan('tiny'));
app.use(cookieParser());
app.use(express.json());

app.use('/api/auth', authRouter);
app.use('/api/users', authorizationMiddleware, userRouter);
app.use('/api/trucks', [authorizationMiddleware, authenticationMiddleware(Roles.DRIVER)], tracksRouter);
app.use('/api/loads', [authorizationMiddleware], loadsRouter);

app.use(asyncErrorHandle(async () => {
  throw new NotFoundError();
}));

app.use((err, req, res, next) => {
  const resStatus = err.status || 500;
  const resMessage = err.message || 'Internal server error';
  console.log(err);
  res.status(resStatus).json({message: resMessage});
});

(async () => {
  await serveApp();
})();


/** Connect to BD and listen to port */
async function serveApp() {
  try {
    await connectToBD();
    app.listen(port);
  } catch (error) {
    console.log(error);
  }
}

/** Connect to mongo DB */
async function connectToBD() {
  await mongoose.connect(dbConnectionUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  });
  console.log('DB connected');
}
