const Roles = {
  'SHIPPER': 'SHIPPER',
  'DRIVER': 'DRIVER',
};

module.exports = {
  Roles: Roles,
};
