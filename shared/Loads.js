const LoadStatuses = {
  'NEW': 'NEW', // For just create, but not posted yet loads
  'POSTED': 'POSTED', // User posted his load, searching for driver
  'ASSIGNED': 'ASSIGNED', // Driver found and assigned
  'SHIPPED': 'SHIPPED', // Finished shipment, history
};

const LoadStates = {
  'EN_ROUTE_TO_PICK_UP': 'En route to Pick Up',
  'ARRIVED_TO_PICK_UP': 'Arrived to Pick Up',
  'EN_ROUTE_TO_DELIVERY': 'En route to delivery',
  'ARRIVED_TO_DELIVERY': 'Arrived to delivery',
};

module.exports = {
  LoadStatuses,
  LoadStates,
};
