const Joi = require('joi');
const {LoadStates, LoadStatuses} = require('./Loads');
const {Trucks, TruckStatuses} = require('./Trucks');
const {Roles} = require('./Roles');

const JoiSchemas = {
  username: Joi.string().min(2),
  password: Joi.string().min(4),
  encryptedPassword: Joi.string().length(60)
      .error(() => new Error('Password should be encrypted')),
  email: Joi.string().email(),
  role: Joi.string().valid(Roles.DRIVER, Roles.SHIPPER),
  date: Joi.date(),
  limit: Joi.string().pattern(/^[0-9]+$/),
  offset: Joi.string().pattern(/^[0-9]+$/),
};

const JoiTruckSchemas = {
  type: Joi.string().valid(...Object.keys(Trucks)),
  status: Joi.string().valid(...Object.keys(TruckStatuses)).allow(null),
};


const JoiLoadSchemas = {
  statuses: Joi.string().valid(...Object.keys(LoadStatuses)),
  states: Joi.string().valid(...Object.values(LoadStates)).allow(null),
  name: Joi.string().min(2),
  payload: Joi.number(),
  pickup_address: Joi.string(),
  delivery_address: Joi.string(),
  dimensions: Joi.object({
    width: Joi.number().required(),
    length: Joi.number().required(),
    height: Joi.number().required(),
  }),
  logs: Joi.array().items(Joi.object({
    message: Joi.string(),
    time: Joi.date(),
  })),
};

module.exports = {
  JoiSchemas,
  JoiTruckSchemas,
  JoiLoadSchemas,
};
